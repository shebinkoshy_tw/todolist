# DB SETUP
Execute postgress query `CREATE DATABASE todolist_mgmt;` to create DB. 

# How to test functionality?
**Hosted API:** 
* Open your postman application
* Import -> Import from link https://www.getpostman.com/collections/ae8c84ae47860c17b3b8

**To test Locally hosted API:** 
Open the postman and import `Todolist assignment.postman_collection.json`


# Tech stack
* Spring Boot Rest layer
* Java Persistence API
* JSON Web Token (JWT) for authentication
* Postgres DB
* Liquibase for DB Initialization
* Heroku for hosting


# Pending
* JWT Token expiry 
* Improve error handling in more better way
* Create web-service to fetch todolist items with date along with status and priority and priority order (ascending & descending)
* Test cases
* More clean code
