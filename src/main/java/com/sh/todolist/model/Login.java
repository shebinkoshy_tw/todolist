package com.sh.todolist.model;

import lombok.Data;
import javax.validation.constraints.NotBlank;

@Data
public class Login {

    @NotBlank(message = "Name is mandatory")
    private  String username;


    @NotBlank(message = "Password is mandatory")
    private  String password;
}
