package com.sh.todolist.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="priority", schema = "preferenceschema")
@Data
public class Priority {

        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        private int id;

        @NotNull(message = "Item name cannot be null")
        @Column(name = "name", nullable = false, unique = true)
        private  String name;

        @Min(1)
        @Column(name = "priority_order", nullable = false, unique = true)
        private  int priorityOrder;
}
