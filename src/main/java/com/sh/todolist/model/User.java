package com.sh.todolist.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Entity
@Table(name="user", schema = "userschema")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotNull(message = "Username cannot be null")
    @Column(name = "username", nullable = false, unique = true)
    private  String username;

    @NotNull(message = "Password cannot be null")
    @Column(name = "password", nullable = false)
    @Getter(AccessLevel.NONE)
    private  String password;

    @JsonIgnore
    @Column(name = "created_on", updatable = false)
    private Timestamp createdOn;

    @NotNull(message = "Name cannot be null")
    @Column(name = "name", nullable = false)
    private  String name;


    @PrePersist
    protected void onCreate() {
        LocalDateTime ldt = LocalDateTime.now();
        ZonedDateTime zdt = ZonedDateTime.of(ldt, ZoneId.systemDefault());
        ZonedDateTime gmt = zdt.withZoneSameInstant(ZoneId.of("GMT"));
        Timestamp timestamp = Timestamp.valueOf(gmt.toLocalDateTime());
        if (createdOn == null) { createdOn = timestamp; }
    }


    public Timestamp getCreatedAt() {
        return createdOn;
    }


    @JsonIgnore//Used for UserPrinciple object
    public String getPassword() {
        return password;
    }
}
