package com.sh.todolist.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="status", schema = "preferenceschema")
@Data
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotNull(message = "Item name cannot be null")
    @Column(name = "name", nullable = false, unique = true)
    private  String name;

}
