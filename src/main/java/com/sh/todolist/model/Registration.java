package com.sh.todolist.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class Registration {

    @NotBlank(message = "Username is mandatory")
    private  String username;


    @NotBlank(message = "Password is mandatory")
    private  String password;

    @NotBlank(message = "Name is mandatory")
    private  String name;
}
