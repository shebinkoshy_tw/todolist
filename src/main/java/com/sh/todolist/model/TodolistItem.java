package com.sh.todolist.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Table(name="todolist", schema = "todolistschema")
@Data
public class TodolistItem {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotNull(message = "Item name cannot be null")
    @Column(name = "item_name", nullable = false, unique = true)
    private  String itemName;

    @Column(name = "item_description")
    private  String itemDescription;


    @NotNull(message = "Firing time cannot be null")
    @Column(name = "firing_time")
    private Timestamp firingTime;


    @JsonIgnore//Ignored as while creating user won't provide user id, java code will assign from principal object.
    @Column(name = "user_id", nullable = false)
    private int userId;


    @Min(1)
    @Column(name = "priority_id", nullable = false)
    private int priorityId;


    @Min(1)
    @Column(name = "status_id", nullable = false)
    private int statusId;


    //Since 'userId' property is @JsonIgnore, to return userId on GET web-service.
    public int getUserIdentity() {
        return userId;
    }
}
