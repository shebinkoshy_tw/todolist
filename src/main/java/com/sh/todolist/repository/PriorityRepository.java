package com.sh.todolist.repository;

import com.sh.todolist.model.Priority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PriorityRepository extends JpaRepository<Priority, Integer> {

    Optional<Priority> findByPriorityOrder(Integer orderId);

    Optional<Priority> findByName(String name);
}
