package com.sh.todolist.repository;

import com.sh.todolist.model.Priority;
import com.sh.todolist.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StatusRepository extends JpaRepository<Status, Integer> {

    Optional<Status> findByName(String name);
}