package com.sh.todolist.repository;

import com.sh.todolist.model.TodolistItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;

public interface TodolistRepository extends JpaRepository<TodolistItem, Integer> {

    List<TodolistItem> findAllByUserId(Integer integers);

    @Modifying
    @Query("UPDATE TodolistItem t set t.itemName = :itemName, t.itemDescription = :itemDescription, t.firingTime = :firingTime, t.priorityId = :priorityId where t.id = :id and t.userId = :userId")
    int updateEmployeeById(@Param("itemName") String itemName, @Param("itemDescription") String itemDescription, @Param("firingTime") Timestamp firingTime, @Param("priorityId") int priorityId, @Param("id") int id, @Param("userId") int userId);

    @Query("from TodolistItem t where t.id = :id and t.userId = :userId")
    TodolistItem findByItemIdAndUserId(Integer id, Integer userId);

    @Query("select t from TodolistItem t where t.statusId = :statusId and t.userId = :userId")
    List<TodolistItem> findAllByStatusIdAndUserId(Integer statusId, Integer userId);

    @Query("select t from TodolistItem t where t.priorityId = :priorityId and t.userId = :userId")
    List<TodolistItem> findAllByPriorityIdAndUserId(Integer priorityId, Integer userId);

    @Query("select t from TodolistItem t INNER JOIN Priority p ON t.priorityId = p.id AND t.userId = :userId ORDER BY p.priorityOrder ASC")
    List<TodolistItem> findAllByPriorityAcendingOrderAndUserId(Integer userId);

    @Query("select t from TodolistItem t INNER JOIN Priority p ON t.priorityId = p.id AND t.userId = :userId ORDER BY p.priorityOrder DESC")
    List<TodolistItem> findAllByPriorityDecendingOrderAndUserId(Integer userId);

    @Query("select t from TodolistItem t where t.userId = :userId and t.firingTime between :startDateAndTime and :endDateAndTime")
    List<TodolistItem> findAllBetweenDatesWithUserId(Timestamp startDateAndTime, Timestamp endDateAndTime, Integer userId);

    @Modifying
    @Query("DELETE TodolistItem t where t.id = :id and t.userId = :userId")
    int deleteByItemIdAndUserId(@Param("id") int id, @Param("userId") int userId);

}
