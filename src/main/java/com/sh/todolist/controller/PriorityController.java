package com.sh.todolist.controller;

import com.sh.todolist.model.Priority;
import com.sh.todolist.service.PriorityService;
import com.sh.todolist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RequestMapping(value = "/priority")
@RestController
public class PriorityController extends BaseController {

    @Autowired
    PriorityService priorityService;

    @Autowired
    UserService userService;

    @PostMapping("")
    public Object createPriority(@Valid @RequestBody Priority priority, Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        Optional<Priority> exist = priorityService.findByPriorityOrder(priority.getPriorityOrder());
        if (exist != null && exist.isPresent()) {
            return duplicateEntryResponseEntity("Priorityorder already exist");
        }
        Optional<Priority> exist2 = priorityService.fetchByName(priority.getName());
        if (exist2 != null && exist2.isPresent()) {
            return duplicateEntryResponseEntity("Name already exist");
        }
        Priority created = priorityService.createPriority(priority);
        if (created == null) {
            return badRequestErrorResponseEntity("Something went wrong");
        }
        return successResponseEntity(created);
    }

    @GetMapping("")
    public Map<String, Object> listAllPriorities() {
        List<Priority> list = priorityService.listAllPriorities();
        return successResponseEntity(list);
    }
}