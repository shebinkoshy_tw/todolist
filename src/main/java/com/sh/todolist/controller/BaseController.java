package com.sh.todolist.controller;

import com.sh.todolist.model.User;
import com.sh.todolist.model.UserPrincipal;
import com.sh.todolist.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

public class BaseController {

    private static final String USER_NOTFOUND = "User not found. Please check token";

    private boolean isCurrentUserId(Integer userId, Principal principal, UserService userService) {
        int userPrincipalId = currentUserId(principal, userService);
        if (userId == userPrincipalId) {
            return true;
        }
        return false;
    }

    protected int currentUserId(Principal principal, UserService userService) {
        if (principal instanceof UsernamePasswordAuthenticationToken) {
            String username = (String) ((UsernamePasswordAuthenticationToken) principal).getPrincipal();
            User user = userService.findByUsername(username);
            if (user != null) {
                return user.getId();
            }
        }
        if (principal instanceof UserPrincipal) {
            int userPrincipalId = ((UserPrincipal) principal).getUserId();
            return userPrincipalId;
        }
        return -1;
    }

    //404
    protected ResponseEntity<Object> userNotFoundResponseEntity() {
        return badRequestErrorResponseEntity(USER_NOTFOUND);
    }

    //409
    protected ResponseEntity<Object> duplicateEntryResponseEntity(String message) {
        HashMap<String, String> map = new HashMap<>();
        map.put("error", message);
        return new ResponseEntity<>(map, HttpStatus.CONFLICT);
    }

    //404
    protected ResponseEntity<Object> badRequestErrorResponseEntity(String message) {
        HashMap<String, String> map = new HashMap<>();
        map.put("error", message);
        return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
    }


    protected Map<String, String> successResponseEntity() {
        HashMap<String, String> map = new HashMap<>();
        map.put("message", "success");
        return map;
    }

    protected Map<String, Object> successResponseEntity(Object object) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("message", "success");
        map.put("data", object);
        return map;
    }

}
