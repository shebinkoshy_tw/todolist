package com.sh.todolist.controller;

import com.sh.todolist.model.*;
import com.sh.todolist.service.PriorityService;
import com.sh.todolist.service.StatusService;
import com.sh.todolist.service.TodolistService;
import com.sh.todolist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/todolist")
@RestController
public class TodolistController extends BaseController {

    @Autowired
    TodolistService todolistService;

    @Autowired
    UserService userService;

    @Autowired
    PriorityService priorityService;

    @Autowired
    StatusService statusService;

    @PostMapping("")
    public Object createTodolistItem(@Valid  @RequestBody TodolistItem item, Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        Optional<Priority> priority = priorityService.fetchById(item.getPriorityId());
        if (priority == null || priority.isPresent() == false) {
            return badRequestErrorResponseEntity("Please enter valid priorityId");
        }
        Optional<Status> status = statusService.fetchById(item.getStatusId());
        if (status == null || status.isPresent() == false) {
            return badRequestErrorResponseEntity("Please enter valid statusId");
        }
        item.setUserId(currentUserId);
        TodolistItem created = todolistService.createTodolistItem(item);
        if (created == null) {
            return badRequestErrorResponseEntity("Something went wrong");
        }
        return successResponseEntity(created);
    }

    //TODO: Comment this code when goes live. or create admin user and restrict to admin
    @GetMapping("publicAll")
    public Object publicListAllTodolistItems() {
        List<TodolistItem> list = todolistService.listAllTodolistItems();
        return successResponseEntity(list);
    }


    @GetMapping("")
    public Object listAllTodolistItems(Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        List<TodolistItem> list = todolistService.listAllTodolistItemsByUserId(currentUserId);
        return successResponseEntity(list);
    }



    @GetMapping("/status/{statusId}")
    public Object listTodolistItemsByStatusIdAndUserId(@PathVariable("statusId") Integer statusId , Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        List<TodolistItem> list = todolistService.listTodolistItemsByStatusIdAndUserId(statusId, currentUserId);
        return successResponseEntity(list);
    }


    @GetMapping("/priority/{priorityId}")
    public Object listTodolistItemsByPriorityId(@PathVariable("priorityId") Integer priorityId , Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        List<TodolistItem> list = todolistService.listTodolistItemsByPriorityIdAndUserId(priorityId, currentUserId);
        return successResponseEntity(list);
    }

    @GetMapping("/priority")
    public Object listTodolistItemsByPriorityOrder(@RequestParam(value = "priorityOrderAscending", required = true) boolean isPriorityOrderAscending , Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        List<TodolistItem> list = todolistService.listTodolistItemsByPriorityOrderAndUserId(isPriorityOrderAscending, currentUserId);
        return successResponseEntity(list);
    }

    @GetMapping("/betweendate")
    public Object listTodolistItemsBetweenDates(
            @RequestParam(name="startdate", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date startDateAndTime,
            @RequestParam(name="enddate", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date endDateAndTime, Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        List<TodolistItem> list = todolistService.listTodolistItemsBetweenDatesWithUserId(startDateAndTime, endDateAndTime, currentUserId);
        return successResponseEntity(list);
    }

    @GetMapping("/{id}")
    public Object listTodolistItem(@PathVariable("id") Integer itemId, Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        TodolistItem item = todolistService.todolistItemByIdAndUserId(itemId, currentUserId);
        if (item == null) {
            return badRequestErrorResponseEntity("Item not exist");
        }
        return successResponseEntity(item);
    }

    @PutMapping("/{id}")
    public Object updateTodolistItem(@PathVariable("id") Integer itemId,@Valid @RequestBody TodolistItem item, Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        TodolistItem existingItem = todolistService.todolistItemByIdAndUserId(itemId, currentUserId);
        String message = "Failed";
        if (existingItem != null) {
            int isSuccess = todolistService.updateTodolistItem(itemId, currentUserId, item);
            if (isSuccess == 1) return successResponseEntity();
        } else {
            message = "Item not exist";
        }
        return badRequestErrorResponseEntity(message);
    }

    @DeleteMapping("/{id}")
    public Object deleteTodolistItem(@PathVariable("id") Integer itemId, Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        TodolistItem existingItem = todolistService.todolistItemByIdAndUserId(itemId, currentUserId);
        String message = "Failed";
        if (existingItem != null) {
            int isSuccess = todolistService.deleteTodolistItem(itemId, currentUserId);
            if (isSuccess == 1) return successResponseEntity();
        } else  {
            message = "Item not exist";
        }
        return badRequestErrorResponseEntity(message);
    }


}
