package com.sh.todolist.controller;

import com.sh.todolist.model.Registration;
import com.sh.todolist.model.User;
import com.sh.todolist.service.SecurityServiceImpl;
import com.sh.todolist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class UserController extends BaseController {

    @Autowired
    UserService userService;

    @Autowired
    SecurityServiceImpl securityService;


    @PostMapping("/registration")
    public Object registration(@Valid @RequestBody Registration registration) {
        User exist = userService.findByUsername(registration.getUsername());
        if (exist != null) {
            return duplicateEntryResponseEntity("Username already exist");
        }
        User user = userService.createUser(registration);
        if (user == null) {
            return badRequestErrorResponseEntity("Something went wrong");
        }
        return successResponseEntity(user);
    }


    //TODO: Comment this code when goes live.
    @GetMapping("/publicAllUser")
    public Map<String, Object> listAllUsers() {
        List<User> list = userService.listAllUsers();
        return successResponseEntity(list);
    }



}