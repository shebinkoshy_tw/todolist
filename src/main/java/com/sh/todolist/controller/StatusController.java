package com.sh.todolist.controller;

import com.sh.todolist.model.Status;
import com.sh.todolist.service.StatusService;
import com.sh.todolist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RequestMapping(value = "/status")
@RestController
public class StatusController extends BaseController {

    @Autowired
    StatusService statusService;

    @Autowired
    UserService userService;

    @PostMapping("")
    public Object createStatus(@Valid @RequestBody Status status, Principal principal) {
        int currentUserId = currentUserId(principal, userService);
        if (currentUserId == -1) {
            return userNotFoundResponseEntity();
        }
        Optional<Status> exist = statusService.fetchByName(status.getName());
        if (exist != null && exist.isPresent()) {
            return duplicateEntryResponseEntity("Name already exist");
        }
        Status created = statusService.createStatus(status);
        if (created == null) {
            return badRequestErrorResponseEntity("Something went wrong");
        }
        return successResponseEntity(created);
    }

    @GetMapping("")
    public Map<String, Object> listAllStatus() {
        List<Status> list = statusService.listAllStatus();
        return successResponseEntity(list);
    }
}
