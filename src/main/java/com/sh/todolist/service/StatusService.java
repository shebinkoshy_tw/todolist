package com.sh.todolist.service;
;
import com.sh.todolist.model.Status;
import com.sh.todolist.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StatusService {

    @Autowired
    StatusRepository statusRepository;

    public Status createStatus(Status status) {
        return statusRepository.save(status);
    }

    public List<Status> listAllStatus() {
        return statusRepository.findAll();
    }

    public Optional<Status> fetchById(Integer id) {
        return statusRepository.findById(id);
    }

    public Optional<Status> fetchByName(String name) {
        return statusRepository.findByName(name);
    }
}
