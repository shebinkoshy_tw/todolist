package com.sh.todolist.service;

import com.sh.todolist.model.User;
import com.sh.todolist.model.UserPrincipal;
import com.sh.todolist.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;



@Service
public class UserPrincipalDetailsServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    public UserPrincipalDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) throw new UsernameNotFoundException(username);

        UserPrincipal userPrincipal = new UserPrincipal(user);
        return userPrincipal;
    }
}
