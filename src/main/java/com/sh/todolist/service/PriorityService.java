package com.sh.todolist.service;

import com.sh.todolist.model.Priority;
import com.sh.todolist.repository.PriorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PriorityService {
    @Autowired
    PriorityRepository priorityRepositoryImpl;

    public Priority createPriority(Priority priority) {
        return priorityRepositoryImpl.save(priority);
    }

    public List<Priority> listAllPriorities() {
        return priorityRepositoryImpl.findAll();
    }

    public Optional<Priority> fetchById(Integer id) {
        return priorityRepositoryImpl.findById(id);
    }

    public Optional<Priority> findByPriorityOrder(Integer priorityOrder) {
        return priorityRepositoryImpl.findByPriorityOrder(priorityOrder);
    }

    public Optional<Priority> fetchByName(String name) {
        return priorityRepositoryImpl.findByName(name);
    }
}
