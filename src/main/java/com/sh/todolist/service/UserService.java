package com.sh.todolist.service;

import com.sh.todolist.model.Registration;
import com.sh.todolist.model.User;
import com.sh.todolist.repository.UserRepository;
import com.sh.todolist.security.MyPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    MyPasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepositoryImpl;


    public User createUser(Registration registration) {
        User user = new User();
        user.setName(registration.getName());
        user.setUsername(registration.getUsername());
        String encryptedPwd = passwordEncoder.encode(registration.getPassword());
        user.setPassword(encryptedPwd);
        return userRepositoryImpl.save(user);
    }

    public User findByUsername(String username) {
        return userRepositoryImpl.findByUsername(username);
    }

    public User findByUsernamePassword(String username, String password) {
        return userRepositoryImpl.findByUsernameAndPassword(username, password);
    }

    //TODO: Comment this code when goes live.
    public List<User> listAllUsers() {
        return userRepositoryImpl.findAll();
    }


}
