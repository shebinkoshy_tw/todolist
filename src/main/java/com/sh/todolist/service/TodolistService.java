package com.sh.todolist.service;

import com.sh.todolist.model.TodolistItem;
import com.sh.todolist.repository.TodolistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class TodolistService {

    @Autowired
    TodolistRepository todolistRepositoryImpl;


    public TodolistItem todolistItemByIdAndUserId(int itemId, int userId) {
        return todolistRepositoryImpl.findByItemIdAndUserId(itemId, userId);
    }

    public TodolistItem createTodolistItem(TodolistItem item) {
        return todolistRepositoryImpl.save(item);
    }

    //TODO: Comment this code when goes live.
    public List<TodolistItem> listAllTodolistItems() {
        return todolistRepositoryImpl.findAll();
    }

    public List<TodolistItem> listAllTodolistItemsByUserId(int userId) {
        return todolistRepositoryImpl.findAllByUserId(userId);
    }

    public List<TodolistItem> listTodolistItemsByStatusIdAndUserId(int statusId, int userId) {
        return todolistRepositoryImpl.findAllByStatusIdAndUserId(statusId, userId);
    }

    public List<TodolistItem> listTodolistItemsByPriorityIdAndUserId(int priorityId, int userId) {
        return todolistRepositoryImpl.findAllByPriorityIdAndUserId(priorityId, userId);
    }

    public List<TodolistItem> listTodolistItemsByPriorityOrderAndUserId(boolean isPriorityOrderAscending, int userId) {
        if (isPriorityOrderAscending == true) {
            return todolistRepositoryImpl.findAllByPriorityAcendingOrderAndUserId(userId);
        }
        return todolistRepositoryImpl.findAllByPriorityDecendingOrderAndUserId(userId);
    }

    public List<TodolistItem> listTodolistItemsBetweenDatesWithUserId(Date startDateAndTime, Date endDateAndTime, int userId) {
        Timestamp start=new Timestamp(startDateAndTime.getTime());
        Timestamp end=new Timestamp(endDateAndTime.getTime());
        return todolistRepositoryImpl.findAllBetweenDatesWithUserId(start, end, userId);
    }


    @Transactional
    public int updateTodolistItem(Integer itemId, Integer userId, TodolistItem item) {
        item.setUserId(userId);//May not be required. but still doing.
         return todolistRepositoryImpl.updateEmployeeById(item.getItemName(), item.getItemDescription(), item.getFiringTime(), item.getPriorityId(), itemId, userId);
    }

    @Transactional
    public int deleteTodolistItem(Integer id, Integer userId) {
        return todolistRepositoryImpl.deleteByItemIdAndUserId(id, userId);
    }
}
