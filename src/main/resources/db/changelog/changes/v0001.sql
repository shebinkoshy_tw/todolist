
CREATE SEQUENCE public.hibernate_sequence INCREMENT 1 START 1 MINVALUE 1;

CREATE SCHEMA userschema;

CREATE TABLE userschema.user (
    id SERIAL PRIMARY KEY    NOT NULL,
   username TEXT   UNIQUE     NOT NULL,
   password TEXT NOT NULL,
   created_on timestamp without time zone default (now() at time zone 'utc'),
   name TEXT NOT NULL
);

CREATE SCHEMA preferenceschema;

CREATE TABLE preferenceschema.status (
    id SERIAL PRIMARY KEY    NOT NULL,
   name TEXT   UNIQUE     NOT NULL
);

CREATE TABLE preferenceschema.priority (
    id SERIAL PRIMARY KEY    NOT NULL,
   name TEXT   UNIQUE     NOT NULL,
   priority_order INT UNIQUE NOT NULL
);

CREATE SCHEMA todolistschema;

CREATE TABLE todolistschema.todolist (
    id SERIAL PRIMARY KEY    NOT NULL,
   item_name TEXT     NOT NULL,
   item_description TEXT,
   firing_time timestamp without time zone NOT NULL,
   user_id  INT   NOT NULL  REFERENCES userschema.user(id),
   priority_id  INT  NOT NULL REFERENCES preferenceschema.priority(id),
   status_id INT NOT NULL REFERENCES preferenceschema.status(id)
);


INSERT INTO preferenceschema.status (name) VALUES ('Created');
INSERT INTO preferenceschema.status (name) VALUES ('Completed');

INSERT INTO preferenceschema.priority (name,priority_order) VALUES ('High',3);
INSERT INTO preferenceschema.priority (name,priority_order) VALUES ('Low',1);
INSERT INTO preferenceschema.priority (name,priority_order) VALUES ('Medium',2);